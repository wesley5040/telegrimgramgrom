# Makefile for Telegrimgramgrom
# Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

################################################################################
####################################  VARS  ####################################
################################################################################

# Compilation stuff
CC = gcc         # C Compiler
CF = -Wall -c -g # Compile time flags
LF = -Wall -g -lcrypto   # Link time flags

# Final executables
CLI_CLIENT = exes/client_cli
GUI_CLIENT = exes/client_gui
SERVER     = exes/server
TEST = exes/test

# Intermediate shit
COMMON_O        = bin/common.o
CLIENT_CLI_O    = bin/client_cli.o
CLIENT_KERNEL_O = bin/client_kernel.o
SERVER_KERNEL_O = bin/server_kernel.o
SERVER_CLI_O    = bin/server_cli.o
SERVER_FILEIO_O = bin/server_fileIO.o
TESTING_O 		= bin/testing.o
# Header files
COMMON_H = src/common/common.h
CLIENT_H = src/client/client.h
SERVER_H = src/server/server.h
# C files
COMMON_C        = src/common/common.c
CLIENT_CLI_C    = src/client/cli.c
CLIENT_KERNEL_C = src/client/kernel.c
SERVER_KERNEL_C = src/server/kernel.c
SERVER_CLI_C    = src/server/cli.c
SERVER_FILEIO_C = src/server/fileIO.c
TESTING_C 	    = src/testing/testing.c


################################################################################
###################################  BUILD  ####################################
################################################################################

all: server-cli client-cli

###########################  Intended for user use  ############################
client-cli: config mkclientdir $(CLI_CLIENT)

server-cli: config mkserverdir $(SERVER) $(TEST)

config:
	mkdir -p bin
	mkdir -p exes

mkclientdir:
	mkdir -p exes/clientFiles

mkserverdir:
	mkdir -p exes/serverFiles
	mkdir -p exes/serverFiles/convos

clean-build:
	rm -rf bin/

clean-all:
	rm -rf bin/ exes/

###########################  Makes the actual build  ###########################
$(CLI_CLIENT): $(CLIENT_CLI_O) $(CLIENT_KERNEL_O) $(COMMON_O)
	$(CC) $(CLIENT_CLI_O) $(CLIENT_KERNEL_O) $(COMMON_O) -o $(CLI_CLIENT) $(LF)

$(SERVER): $(SERVER_CLI_O) $(SERVER_FILEIO_O) $(SERVER_KERNEL_O) $(COMMON_O) 
	$(CC) $(SERVER_CLI_O) $(SERVER_FILEIO_O) $(SERVER_KERNEL_O) $(COMMON_O) -o $(SERVER) $(LF)

$(TEST): $(TESTING_O) $(COMMON_O)
	$(CC) $(TESTING_O) $(COMMON_O) -o $(TEST) $(LF)

$(CLIENT_CLI_O): $(CLIENT_CLI_C) $(CLIENT_H)
	$(CC) $(CLIENT_CLI_C) -o $(CLIENT_CLI_O) $(CF)

$(CLIENT_KERNEL_O): $(CLIENT_KERNEL_C) $(CLIENT_H)
	$(CC) $(CLIENT_KERNEL_C) -o $(CLIENT_KERNEL_O) $(CF)

$(SERVER_CLI_O): $(SERVER_CLI_C) $(SERVER_H)
	$(CC) $(SERVER_CLI_C) -o $(SERVER_CLI_O) $(CF)

$(SERVER_FILEIO_O): $(SERVER_FILEIO_C) $(SERVER_H)
	$(CC) $(SERVER_FILEIO_C) -o $(SERVER_FILEIO_O) $(CF)

$(SERVER_KERNEL_O): $(SERVER_KERNEL_C) $(SERVER_H)
	$(CC) $(SERVER_KERNEL_C) -o $(SERVER_KERNEL_O) $(CF)

$(COMMON_O): $(COMMON_C) $(COMMON_H)
	$(CC) $(COMMON_C) -o $(COMMON_O) $(CF)

$(TESTING_O): $(TESTING_C) $(COMMON_H)
	$(CC) $(TESTING_C) -o $(TESTING_O) $(CF)

