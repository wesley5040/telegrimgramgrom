/* client.c
 * Wesley Van Pelt and Максим Заман
 * Taken from Lab1 in CSSE432
 */

// #include "./globals.h"
#include "../common/common.h"

static unsigned long resolveName(const char* name);

int main(int argc, char* argv[]) {
  int ch, sockit, connection, isVerbose, msgLen;
  unsigned short serverPort;
  char *serverName, *netBuf;
  struct sockaddr_in serverAddr;

  serverName = "localhost";
  serverPort = 31337;
  isVerbose = 1;

  while ((ch = getopt(argc, argv, "h:p:uv")) != -1) {
    switch (ch) {
      case 'h':
        serverName = optarg;
        break;
      case 'p':
        serverPort = atoi(optarg);
        break;
      case 'v':
        isVerbose = 1;
        break;
      case 'u':
      default:
        fprintf(stderr, "Usage: client [-u] [-v] [-h <server>] [-p <port>]\n");
        return 0;
    }
  }
  if (isVerbose) {
    printf("Using server %s\n", serverName);
    printf("Using port %d\n", serverPort);
  }

  //Create the socket
  if ((sockit = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    printf("Socket creation failed D:< \n");
    return 1;
  }

  //Initialize the endpoint structure
  memset(&serverAddr, 0, sizeof(serverAddr));
  serverAddr.sin_family      = AF_INET;
  serverAddr.sin_addr.s_addr = resolveName(serverName);
  serverAddr.sin_port        = htons(serverPort);
  if ((connection = connect(sockit, &serverAddr, sizeof(serverAddr))) < 0) {
    printf("Error connecting to server\n");
    return 1;
  }

  // while (1) {
  //   printf("Enter input string> ");
  //   scanf("%[^\n]%*c", input);
  //   if (input[0] == ';' && input[1] == ';' && input[2] == ';' && input[3] == '\0') {
  //     printf("exiting\n");
  //     break;
  //   }
  netBuf = malloc(NET_BUFFER_SIZE);

  setEncrypted(netBuf, 1);
  setMsgType(netBuf, NET_HELLOS);

  send(sockit, netBuf, 2, 0);
  msgLen = recv(sockit, netBuf, NET_BUFFER_SIZE, 0);

  printf("mgl: %d\n", msgLen);
  printf("enc: %d\n", getEncrypted(netBuf));
  printf("mgt: %d\n", getMsgType(netBuf));
  printf("kln: %d\n", getNetHellosRes_svrPubkeyLen(netBuf));
  printf("key: %s\n", getNetHellosRes_svrPubkey(netBuf));
  printf("len: %d\n", getNetHellosRes_size(netBuf));

  close(sockit);
  return 0;
}

static unsigned long resolveName(const char* name) {
  struct hostent *host;
  if ((host = gethostbyname(name)) == NULL) {
    perror("gethostbyname() failed");
    exit(1);
  }
  return *((unsigned long*)host->h_addr_list[0]);
}
