#include "../common/common.h"

int main(int argc, char* argv[]) {
  int i, j, *p0, *p1;
  char *netBuf, *uname, *pword;
  time_t ti;

  netBuf = malloc(NET_BUFFER_SIZE);
  uname  = "abcdefghijklmno";
  pword  = "abcdefghijklmnopqrstuvwxyz01234";
  ti = time(NULL);

  setNetEncrypted(netBuf, 1);
  // setNetMsgType(netBuf, NET_HELLOS_RES);
  // setNetHellosRes_svrPubkeyLen(netBuf, 7);
  // setNetHellosRes_svrPubkey(netBuf, "topkek");
  // setNetMsgType(netBuf, NET_MKUSER);
  // setNetMkUser_username(netBuf, uname);
  // setNetMkUser_password(netBuf, pword);
  // setNetMsgType(netBuf, NET_MKUSER_RES);
  // setNetMkUserRes_status(netBuf, 1);
  // setNetMsgType(netBuf, NET_LIUSER);
  // setNetLIUser_username(netBuf, uname);
  // setNetLIUser_password(netBuf, pword);
  // setNetMsgType(netBuf, NET_LIUSER_RES);
  // setNetLIUserRes_status(netBuf, 1337);
  // setNetLIUserRes_userId(netBuf, 420);
  // setNetLIUserRes_utoken(netBuf, 69);
  setNetMsgType(netBuf, NET_SNDMSG);
  setNetSndMsg_toUserId(netBuf, 420);
  setNetSndMsg_frUserId(netBuf, 1337);
  setNetSndMsg_bodyLen(netBuf, 32);
  setNetSndMsg_sendTime(netBuf, ti);
  setNetSndMsg_uToken(netBuf, 69);
  setNetSndMsg_body(netBuf, pword);

  printf("enc: %d\n", getNetEncrypted(netBuf));
  printf("mgt: %d\n", getNetMsgType(netBuf));
  // printf("kln: %d\n", getNetHellosRes_svrPubkeyLen(netBuf));
  // printf("key: %s\n", getNetHellosRes_svrPubkey(netBuf));
  // printf("len: %d\n", getNetHellosRes_size(netBuf));
  // printf("unm: %s\n", getNetMkUser_username(netBuf));
  // printf("pwd: %s\n", getNetMkUser_password(netBuf));
  // printf("len: %d\n", getNetMkUser_size(netBuf));
  // printf("sta: %d\n", getNetMkUserRes_status(netBuf));
  // printf("len: %d\n", getNetMkUserRes_size(netBuf));
  // printf("unm: %s\n", getNetLIUser_username(netBuf));
  // printf("pwd: %s\n", getNetLIUser_password(netBuf));
  // printf("len: %d\n", getNetLIUser_size(netBuf));
  // printf("sta: %d\n", getNetLIUserRes_status(netBuf));
  // printf("uid: %d\n", getNetLIUserRes_userId(netBuf));
  // printf("tok: %d\n", getNetLIUserRes_utoken(netBuf));
  // printf("len: %d\n", getNetLIUserRes_size(netBuf));
  printf("tid: %d\n", getNetSndMsg_toUserId(netBuf));
  printf("fid: %d\n", getNetSndMsg_frUserId(netBuf));
  printf("bln: %d\n", getNetSndMsg_bodyLen(netBuf));
  printf("sti: %ld\n", getNetSndMsg_sendTime(netBuf));
  printf("tok: %d\n", getNetSndMsg_uToken(netBuf));
  printf("bod: %s\n", getNetSndMsg_body(netBuf));
  printf("len: %d\n", getNetSndMsg_size(netBuf));

  free(netBuf);
  return 0;
}
