#include "../common/common.h"

int main(int argc, char* argv[]) {
  char plainText[2048] = "Hello";
  unsigned char *encrypted = malloc(64000);
  unsigned char *decrypted = malloc(64000);

  int encrypted_length= public_encrypt((unsigned char*)plainText,strlen(plainText),"./exes/serverFiles/public.pem",encrypted);
  if(encrypted_length == -1)
  {
      printLastError("Public Encrypt failed ");
      exit(0);
  }
  printf("Encrypted length =%d\n",encrypted_length);
  printf("Encrypted Text=%s\n", encrypted);
  int decrypted_length = private_decrypt((unsigned char*)encrypted,encrypted_length,"./exes/serverFiles/private.pem", decrypted);
  if(decrypted_length == -1)
  {
      printLastError("Private Decrypt failed ");
      exit(0);
  }
  printf("Decrypted Text =%s\n",decrypted);
  printf("Decrypted Length =%d\n",decrypted_length);


  encrypted_length= private_encrypt((unsigned char*)plainText,strlen(plainText),"./exes/serverFiles/private.pem",encrypted);
  if(encrypted_length == -1)
  {
      printLastError("Private Encrypt failed");
      exit(0);
  }
  printf("Encrypted length =%d\n",encrypted_length);

  decrypted_length = public_decrypt((unsigned char*)encrypted,encrypted_length,"./exes/serverFiles/public.pem", decrypted);
  if(decrypted_length == -1)
  {
      printLastError("Public Decrypt failed");
      exit(0);
  }
  printf("Decrypted Text =%s\n",decrypted);
  printf("Decrypted Length =%d\n",decrypted_length);
    return 0;
}
