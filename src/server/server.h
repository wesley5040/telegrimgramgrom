// Server header for Telegrimgramgrom
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "../common/common.h"
#define CLI_BUFF_SIZE 8
#define MAX_PENDING   128
#define MAX_USERS     128

typedef struct {
  unsigned short port;
  int debugMode;
  int nsaMode;
  char *path;
  char *pubkey;
  unsigned short pubkeyLen;
} Config;

typedef struct {
  int id;
  int pubKey;
  char name[MAX_USERNAME_LEN];
  char pwrd[MAX_PASSWORD_LEN];
} User;

// Interface stuff
int  initServer(int argc, char** argv, Config config);
void getCLIinput(char* buffer);

// File IO shit
int loadConfig(char* path, Config config);
int saveConfig(char* path, Config config, int setupMode, char* cliBuff);
int loadUsers(char* path, User* users, int maxUsers);
int saveUsers(char* path, User* users, int userCount);
int loadAllConvos(char* path, Convo* convos, int maxConvos);
int saveAllConvos(char* path, Convo* convos, int convoCount);
int loadConvo(char* path, Convo convo);
int saveConvo(char* path, Convo convo);
int checkConfig(char* path);
