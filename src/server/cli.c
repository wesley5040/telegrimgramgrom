// Interface for the Telegrimgramgrom server
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "./server.h"

int initServer(int argc, char** argv, Config config) {
  int i, j, setupMode = 0;
  char cliBuff[CLI_BUFF_SIZE], *configPath, *configFileName = "config";

  config.port      = DEFAULT_PORT;
  config.debugMode = 0;
  config.nsaMode   = 0;
  configPath = (char *) malloc(strlen(config.path)+strlen(configFileName)+2);
  for (i = 0; i < strlen(config.path); i++) configPath[i] = config.path[i];
  for (j = 0; j < strlen(configFileName); configPath[i++] = configFileName[j++]);
  configPath[i] = '\0';

  printf("Welcome to the Telegrimgramgrom server! Use '$ ./server help' for usage.\n\n");
  for (i = 1; i < argc; i++) {
    if (!strncmp(argv[i], "help", 4)) {
      printf("Usage:\n");
      printf("$ ./server [options]\n\n");
      printf("Note that using these arguments will override whatever is in the config:\n");
      printf("  help    : Displays this message and exits\n");
      printf("  setup   : Forces initial setup\n");
      printf("  debug   : Will print debugging code\n");
      printf("  NSA     : Ultra-debug mode\n");
      printf("  port=XXX: Listen on port XXX\n\n");
      return -1;
    }

    if (!strncmp(argv[i], "debug", 5)) config.debugMode = 1;
    if (!strncmp(argv[i], "NSA",   3)) config.nsaMode = 1;
    if (!strncmp(argv[i], "setup", 5)) setupMode = 1;
    if (!strncmp(argv[i], "port=", 5)) config.port = (unsigned short) atoi(argv[i]+5);
  }
  printf("%s\n", configPath);

  i = checkConfig(configPath);
  if (i == 0) {
    loadConfig(configPath, config);
  }
  else if (i || setupMode) {
    saveConfig(configPath, config, setupMode, cliBuff);
  }

  printf("\n");
  printf("Starting server with the following settings:\n");
  printf("  Debug mode     = %s\n", config.debugMode ? "YES" : "NO");
  printf("  NSA mode       = %s\n", config.nsaMode   ? "YES" : "NO");
  printf("  Listening port = %d\n", config.port);
  printf("\n");

  free(configPath);
  return 0;
}

void getCLIinput(char* buffer) {
  fgets(buffer, CLI_BUFF_SIZE, stdin);
  buffer[strlen(buffer) - 1] = '\0';
}
