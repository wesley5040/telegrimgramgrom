// File IO for the Telegrimgramgrom server
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "./server.h"
FILE* configFile;

int loadConfig(char* path, Config config) {
  configFile = fopen(path, "r");
  fscanf(configFile, "%hu", &(config.port)     );
  fscanf(configFile, "%d",  &(config.debugMode));
  fscanf(configFile, "%d",  &(config.nsaMode)  );
  fclose(configFile);
  return 0;
}

int checkConfig(char* path){
  printf("checking if already has config file...\n");
  configFile = fopen(path, "r");
  if(configFile != NULL) {
    return 0;
  }
  return 1;

}

int saveConfig(char* path, Config config, int setupMode, char* cliBuff) {
  configFile = fopen(path, "r");
  printf("%sEntering setup (^C now to add the config yourself)\n", setupMode ? "" : "No config found. ");

    printf("  listen port (%d)> ", config.port ? config.port : DEFAULT_PORT);
    getCLIinput(cliBuff);
    if (cliBuff[0] != '\0')
      config.port = (unsigned short) atoi(cliBuff);

    printf("  debug mode (%c)> ", (config.debugMode==1) ? 'y' : 'n');
    getCLIinput(cliBuff);
    if (cliBuff[0] != '\0')
      config.debugMode = (cliBuff[0]=='y') ? 1 : config.debugMode;

    printf("  NSA mode (%c)> ", (config.nsaMode==1) ? 'y' : 'n');
    getCLIinput(cliBuff);
    if (cliBuff[0] != '\0')
      config.nsaMode = (cliBuff[0]=='y') ? 1 : config.nsaMode;

    printf("Setup complete! Save config to disk? (y)> ");
    getCLIinput(cliBuff);
    if (cliBuff[0] == '\0' || cliBuff[0] == 'y') {
      configFile = fopen(path, "w+");
      if (configFile == NULL) {
        printf("Error saving config to disk\n");
      } else {
        fprintf(configFile, "%hu\n", config.port     );
        fprintf(configFile, "%d\n",  config.debugMode);
        fprintf(configFile, "%d\n",  config.nsaMode  );
        fclose(configFile);
        printf("Settings saved.\n\n");
      }
    }
    printf("Creating private keys and public keys...\n");
    FILE *key;
    key = fopen("./exes/serverFiles/private.pem", "r");
    if(key) {
    } else {
      printf("keys not found, generating now..\n");
      generateKey("./exes/serverFiles/");
    }
  return 0;
}

int loadUsers(char* path, User* users, int maxUsers) {
  // path of the parent directory for the users file
  // returns the amount of users loaded, -1 on fail
  return 0;
}

int saveUsers(char* path, User* users, int userCount) {
  // same as loadUsers, but returns -1 on Error
  return 0;
}

int loadAllConvos(char* path, Convo* convos, int maxConvos) {
  // Path is for the convo directory, each convo should be its own file
  // return number of convos loaded and -1 on fail
  return 0;
}

int saveAllConvos(char* path, Convo* convos, int convoCount) {
  // Same as load, but returns -1 on error
  return 0;
}

int loadConvo(char* path, Convo convo) {
  // Loads the convo at the path, returns -1 on error
  // Here so all convos dont always have to be in memory
  return 0;
}

int saveConvo(char* path, Convo convo) {
  // Saves the convo at the path, returns -1 on error
  return 0;
}
