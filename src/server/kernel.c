// Core for the Telegrimgramgrom server
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "./server.h"

int main(int argc, char** argv) {
  int i, j, sockit, connection, msgLen, currentUsers = 0;
  char *filesPath = "serverFiles/", *recvBuffer;
  pid_t pid;
  socklen_t length;
  struct sockaddr_in serverInfo, clientInfo;
  Config config;
  User users[MAX_USERS];
  Convo convos[MAX_USERS*MAX_USERS];

  config.path = (char *) malloc(strlen(argv[0])+12);
  for (i = strlen(argv[0])-1; i > -1 && argv[0][i] != '/'; i--);
  for (j = 0; j <= i; j++) config.path[j] = argv[0][j];
  for (i = 0; i < strlen(filesPath); config.path[j++] = filesPath[i++]);
  config.path[j] = '\0';

  i = initServer(argc, argv, config);
  if (i) return i;

  // TESTING CODE, REMOVE ME ///////////////////////////////////////////////////
  config.port = 31340;                                                      ////
  config.pubkey = "TOPKEK";                                                 ////
  config.pubkeyLen = strlen(config.pubkey);                                 ////
  // END TESTING CODE //////////////////////////////////////////////////////////

  bzero((char*) &serverInfo, sizeof(serverInfo));
  serverInfo.sin_family      = AF_INET;
  serverInfo.sin_addr.s_addr = INADDR_ANY;
  serverInfo.sin_port        = htons(config.port);

  if ((sockit = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    printf("Socket creation failed D:< \n");
    return 1;
  }
  if ((bind(sockit, (struct sockaddr*) &serverInfo, sizeof(serverInfo))) < 0) {
    printf("Bind on port %d failed\n", config.port);
    return 2;
  }

  printf("Loading users...");
  if (loadUsers(filesPath, users, MAX_USERS) < 0) {
    printf("Error loading users (buffer not large enough?)\n");
    return 3;
  }
  printf("Done\nLoading convos...");
  // Theres probably going to be memory leak in this if its implemented simply...
  if (loadAllConvos(filesPath, convos, MAX_USERS*MAX_USERS) < 0) {
    printf("Error loading convos\n");
    return 4;
  }
  printf("Done\n");
  
  // create pem files in exes
  


  recvBuffer = (char*) malloc(NET_BUFFER_SIZE);
  listen(sockit, MAX_PENDING);
  printf("Listening...\n");

  while (1) {
    length = sizeof(clientInfo);
    bzero((char*) &clientInfo, sizeof(clientInfo));
    if ((connection = accept(sockit, (struct sockaddr*) &clientInfo, &length)) < 0) {
      printf("Connection accepting failed.\n");
      break;
    }

    if ((pid = fork()) > 0) {
      close(connection);
      continue;
    }
    printf("New client connected.\n");

    while ((msgLen = recv(connection, recvBuffer, NET_BUFFER_SIZE, 0))) {
      // unencrypted message starts with 1, encrypted is >1
      if (getNetEncrypted(recvBuffer) > 1) {
        printf("Decrypting shit\n");
        // Decrypt from recvBuffer[1]
      }
      switch (getNetMsgType(recvBuffer)) {
        case NET_DIENOW:
          printf("Client requested a death.\n");
          goto kms;
        case NET_HELLOS:
          setNetMsgType(recvBuffer, NET_HELLOS_RES);
          setNetHellosRes_svrPubkeyLen(recvBuffer, config.pubkeyLen);
          setNetHellosRes_svrPubkey(recvBuffer, config.pubkey);
          send(connection, recvBuffer, getNetHellosRes_size(recvBuffer), 0);
          break;
        case NET_MKUSER:
          for (i = 0; i < currentUsers; i++) {
            if (strcmp(users[i].name, getNetMkUser_username(recvBuffer)))
              break;
          }
          setNetMsgType(recvBuffer, NET_MKUSER_RES);
          if (i == currentUsers) {
            for (i = 0; i < MAX_USERNAME_LEN; i++)
              users[currentUsers].name[i] = ((char*)getNetMkUser_username(recvBuffer))[i];
            for (i = 0; i < MAX_PASSWORD_LEN; i++)
              users[currentUsers].pwrd[i] = ((char*)getNetMkUser_password(recvBuffer))[i];
            currentUsers++;
            setNetMkUserRes_status(recvBuffer, 0);
          } else {
            setNetMkUserRes_status(recvBuffer, 1);
          }
          send(connection, recvBuffer, getNetMkUserRes_size(recvBuffer), 0);
          break;
        case NET_LIUSER:
          // Check stuff
          // Send back token
          break;
        case NET_GTUSER:
          // Return the userid if that contact exists
          break;
        case NET_GTUHST:
          // Return the users the user has contacted
          break;
        case NET_GTCHST:
          // Return the chat history
          break;
          break;
        case NET_SNDMSG:
          // Re-encrypt message
          // Send the message
          break;
        default:
          printf("Recieved invalid net message type: %d, sending error and closing connection.\n", recvBuffer[0]);
          send(connection, "400: Invalid net message type\r\n", 32, 0);
          goto kms;
      }
    }
    printf("Closing connection with a client.\n");
    kms:
    close(connection);
    close(sockit);
    exit(0);
  }

  free(recvBuffer);
  free(config.path);
  return 0;
}
