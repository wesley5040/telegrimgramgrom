// Client header for Telegrimgramgrom
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "../common/common.h"

int myUserId;

// Most of these will need to have the parameters changed...
int makeUser(char* username, char* password);
int logIn(char* username, char* password);
int getContact(char* username);
int getContactHistory(int userId);
int getChatHistory(int userId);
int sendMessage(int  fromUserId, int  toUserId, char* msg, int  msgLen);
int recvMessage(int* fromUserId, int* toUserId, char* msg, int* msgLen);
