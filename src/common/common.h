// Main header for Telegrimgramgrom
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <stdio.h>

#define DEFAULT_PORT     31337
#define NET_BUFFER_SIZE  65536
#define MAX_MESSAGE_LEN  8192
#define MAX_USERNAME_LEN 16
#define MAX_PASSWORD_LEN 32

// All the network message types
#define NET_DIENOW     -1 // Close the connection
#define NET_HELLOS      0 // Say hi to the server
#define NET_HELLOS_RES  1 // Response for NET_HELLOS
#define NET_MKUSER      2 // Make a new user
#define NET_MKUSER_RES  3 // Response for NET_MKUSER
#define NET_LIUSER      4 // Log in user
#define NET_LIUSER_RES  5 // Response for NET_LIUSER
#define NET_GTUSER      6 // Get a contact by username
#define NET_GTUSER_RES  7 // Response for NET_GTUSER
#define NET_GTUHST      8 // Get a all of the people a user has contacted
#define NET_GTUHST_RES  9 // Response for NET_GTUHST
#define NET_GTCHST     10 // Get the chat history for a contact
#define NET_GTCHST_RES 11 // Response for NET_GTCHST
#define NET_SNDMSG     12 // Send a message
#define NET_SNDMSG_RES 13 // Response for NET_SNDMSG

// Very shitty way of handling network types since casting from char[] to structs isn't allowed...
// At least making all of this was great pointer logic practice

// All net messages
#define getNetEncrypted(buffer)    *buffer
#define setNetEncrypted(buffer, e) *buffer = e
#define getNetMsgType(buffer)      *(buffer+1)
#define setNetMsgType(buffer, t)   *(buffer+1) = t

// NET_HELLOS_RES
#define getNetHellosRes_svrPubkeyLen(buffer)    *((int*)(buffer+2))
#define getNetHellosRes_svrPubkey(buffer)       buffer+2+sizeof(int)
#define setNetHellosRes_svrPubkeyLen(buffer, l) *((int*)(buffer+2)) = l
#define setNetHellosRes_svrPubkey(buffer, key)  for (i=2+sizeof(int), j=0; j<*((int*)(buffer+2)); *(buffer+i++) = *(key+j++))
#define getNetHellosRes_size(buffer)            *((int*)(buffer+2))+sizeof(int)+2

// NET_MKUSER
#define getNetMkUser_username(buffer)        buffer+2
#define getNetMkUser_password(buffer)        buffer+2+MAX_USERNAME_LEN
#define getNetMkUser_pkeyLen(buffer, len)    *((int*)(buffer+2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN))
#define getNetMkUser_pkey(buffer, key)       buffer+2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN+sizeof(int)
#define setNetMkUser_username(buffer, uname) for (i=2, j=0; j<MAX_USERNAME_LEN; *(buffer+i++) = *(uname+j++))
#define setNetMkUser_password(buffer, pword) for (i=2+MAX_USERNAME_LEN, j=0; j<MAX_PASSWORD_LEN; *(buffer+i++) = *(pword+j++))
#define setNetMkUser_pkeyLen(buffer, len)    *((int*)(buffer+2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN)) = len
#define setNetMkUser_pkey(buffer, key)       for (i=2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN+sizeof(int), j=0; j<*((int*)(buffer+2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN)); *(buffer+i++) = *(key+j++))
#define getNetMkUser_size(buffer)            2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN+sizeof(int)+*((int*)(buffer+2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN))
// NET_MKUSER_RES
#define getNetMkUserRes_status(buffer)    *((int*)(buffer+2))
#define setNetMkUserRes_status(buffer, s) *((int*)(buffer+2)) = s  // 0=ok, 1=user exests
#define getNetMkUserRes_size(buffer)      2+sizeof(int)

// NET_LIUSER
#define getNetLIUser_username(buffer)        buffer+2
#define getNetLIUser_password(buffer)        buffer+2+MAX_USERNAME_LEN
#define setNetLIUser_username(buffer, uname) for (i=2, j=0; j<MAX_USERNAME_LEN; *(buffer+i++) = *(uname+j++))
#define setNetLIUser_password(buffer, pword) for (i=2+MAX_USERNAME_LEN, j=0; j<MAX_PASSWORD_LEN; *(buffer+i++) = *(pword+j++))
#define getNetLIUser_size(buffer)            2+MAX_USERNAME_LEN+MAX_PASSWORD_LEN
// NET_LIUSER_RES
#define getNetLIUserRes_status(buffer)      *((int*)(buffer+2))
#define getNetLIUserRes_userId(buffer)      *((int*)(buffer+2+sizeof(int)))
#define getNetLIUserRes_utoken(buffer)      *((int*)(buffer+2+2*sizeof(int)))
#define setNetLIUserRes_status(buffer, s)   *((int*)(buffer+2)) = s // 0=ok, 1=no user, 2=incorrect password
#define setNetLIUserRes_userId(buffer, uid) *((int*)(buffer+2+sizeof(int))) = uid
#define setNetLIUserRes_utoken(buffer, tok) *((int*)(buffer+2+2*sizeof(int))) = tok
#define getNetLIUserRes_size(buffer)        2+3*sizeof(int)

// NET_GTUSER
#define getNetGtUser_username(buffer)        buffer+2
#define setNetGtUser_username(buffer, uname) for (i=2, j=0; j<MAX_USERNAME_LEN; *(buffer+i++) = *(uname+j++))
#define getNetGtUser_size(buffer)            2+sizeof(int)
// NET_GTUSER_RES
#define getNetGtUserRes_userId(buffer)    *((int*)(buffer+2))
#define setNetGtUserRes_userId(buffer, s) *((int*)(buffer+2)) = s  // -1=no user
#define getNetGtUserRes_size(buffer)      2+sizeof(int)

// NET_GTUHST
#define getNetGtUHst_utoken(buffer)      *((int*)(buffer+2))
#define getNetGtUHst_userId(buffer)      *((int*)(buffer+2+sizeof(int)))
#define setNetGtUHst_utoken(buffer, tok) *((int*)(buffer+2)) = tok
#define setNetGtUHst_userId(buffer, uid) *((int*)(buffer+2+sizeof(int))) = uid
#define getNetGtUHst_size(buffer)        2+2*sizeof(int)
// NET_GTUHST_RES
#define getNetGtUHstRes_status(buffer)    *((int*)(buffer+2))
#define setNetGtUHstRes_status(buffer, s) *((int*)(buffer+2)) = s  // -1=error, else=usercount
#define getNetGtUHstRes_size(buffer)      2+sizeof(int)

// NET_GTCHST
#define getNetGtCHst_userId0(buffer)      *((int*)(buffer+2))
#define getNetGtCHst_userId1(buffer)      *((int*)(buffer+2+sizeof(int)))
#define getNetGtCHst_urtoken(buffer)      *((int*)(buffer+2+2*sizeof(int)))
#define setNetGtCHst_userId0(buffer, uid) *((int*)(buffer+2)) = uid // id0 < id1
#define setNetGtCHst_userId1(buffer, uid) *((int*)(buffer+2+sizeof(int))) = uid
#define setNetGtCHst_urtoken(buffer, tok) *((int*)(buffer+2+2*sizeof(int))) = tok
#define getNetGtCHst_size(buffer)         2+3*sizeof(int)
// NET_GTCHST_RES
#define getNetGtCHstRes_status(buffer)    *((int*)(buffer+2))
#define setNetGtCHstRes_status(buffer, s) *((int*)(buffer+2)) = s  // -1=error, else=messagecount
#define getNetGtCHstRes_size(buffer)      2+sizeof(int)

// NET_SNDMSG
#define getNetSndMsg_toUserId(buffer)      *((int*)(buffer+2))
#define getNetSndMsg_frUserId(buffer)      *((int*)(buffer+2+sizeof(int)))
#define getNetSndMsg_bodyLen(buffer)       *((int*)(buffer+2+2*sizeof(int)))
#define getNetSndMsg_uToken(buffer)        *((int*)(buffer+2+3*sizeof(int)))
#define getNetSndMsg_sendTime(buffer)      *((time_t*)(buffer+2+4*sizeof(int)))
#define getNetSndMsg_body(buffer)          buffer+2+4*sizeof(int)+sizeof(time_t)
#define setNetSndMsg_toUserId(buffer, uid) *((int*)(buffer+2)) = uid
#define setNetSndMsg_frUserId(buffer, uid) *((int*)(buffer+2+sizeof(int))) = uid
#define setNetSndMsg_bodyLen(buffer, len)  *((int*)(buffer+2+2*sizeof(int))) = len
#define setNetSndMsg_uToken(buffer, tok)   *((int*)(buffer+2+3*sizeof(int))) = tok
#define setNetSndMsg_sendTime(buffer, ti)  *((time_t*)(buffer+2+4*sizeof(int))) = ti
#define setNetSndMsg_body(buffer, body)    for (i=2+4*sizeof(int)+sizeof(time_t), j=0; j<*((int*)(buffer+2+2*sizeof(int))); *(buffer+i++) = *(body+j++))
#define getNetSndMsg_size(buffer)          2+4*sizeof(int)+sizeof(time_t)+*((int*)(buffer+2+2*sizeof(int)))
// NET_SNDMSG_RES
#define getNetSndMsgRes_status(buffer)    *((int*)(buffer+2))
#define setNetSndMsgRes_status(buffer, s) *((int*)(buffer+2)) = s  // -1=error, 0=ok
#define getNetSndMsgRes_size(buffer)      2+sizeof(int)

// Other general structs

typedef struct {
  int idTo;
  char* body;
} Msg;

typedef struct {
  int userId0;
  int userId1;
  int totalMsgs;
  Msg* msgs;
} Convo;

// Encription functions
// Note the key datatype will have to be changed during implementation
RSA * createRSA(char* fileName,int public);
int public_encrypt(unsigned char * data,int data_len, char * key, unsigned char *encrypted);
int private_decrypt(unsigned char * enc_data,int data_len, char * key, unsigned char *decrypted);
int private_encrypt(unsigned char * data,int data_len, char * key, unsigned char *encrypted);
int public_decrypt(unsigned char * enc_data,int data_len, char * key, unsigned char *decrypted);
void printLastError(char *msg);
void generateKey(char* path);

int genKey();
