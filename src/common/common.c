// Generic common stuff for Telegrimgramgrom
// Spring 2017 - Yuzong Gao, Wesley Van Pelt, and Maxim Zaman

#include "./common.h"

int padding = RSA_PKCS1_PADDING;
 
RSA * createRSA(char* fileName,int public)
{
    FILE* file = fopen(fileName, "rb");
    if (file == NULL){
        printf("pem file not found");
        return NULL;
    }
    RSA *rsa= RSA_new();
    if(public)
    {
        rsa = PEM_read_RSA_PUBKEY(file, &rsa,NULL, NULL);
    }
    else
    {
        rsa = PEM_read_RSAPrivateKey(file, &rsa,NULL, NULL);
    }
    if(rsa == NULL)
    {
        printf( "Failed to create RSA");
    }
 
    return rsa;
}
 
int public_encrypt(unsigned char * data,int data_len, char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,1);
    int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int private_decrypt(unsigned char * enc_data,int data_len, char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,0);
    int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
 
int private_encrypt(unsigned char * data,int data_len, char * key, unsigned char *encrypted)
{
    RSA * rsa = createRSA(key,0);
    int result = RSA_private_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}
int public_decrypt(unsigned char * enc_data,int data_len, char * key, unsigned char *decrypted)
{
    RSA * rsa = createRSA(key,1);
    int  result = RSA_public_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
 
void printLastError(char *msg)
{
    char * err = malloc(130);;
    ERR_load_crypto_strings();
    ERR_error_string(ERR_get_error(), err);
    printf("%s ERROR: %s\n",msg, err);
    free(err);
}

void generateKey(char *path){

    char* privateKey = malloc(64);
    char* publicKey = malloc(64);
    char command1[] = "openssl genrsa -out 2048";
    char command2[] = "openssl rsa -in -outform PEM -pubout -out ";
    char* finalCommand1 = malloc(100);
    char* finalCommand2 = malloc(100);

    strcpy(privateKey, path);
    strcat(privateKey, "private.pem");
    printf("command: %s\n", command1);
    strncpy(finalCommand1, command1, 20);
    strcat(finalCommand1, privateKey);
    strcat(finalCommand1, command1+19);
    printf("command: %s\n", finalCommand1);
    strcpy(publicKey, path);
    strcat(publicKey, "public.pem");
    printf("public: %s\n", publicKey);
    strncpy(finalCommand2, command2, 16);
    strcat(finalCommand2, privateKey);
    printf("command: %s\n", finalCommand2);
    strcat(finalCommand2, command2+15);
    strcat(finalCommand2, publicKey);
    strcat(finalCommand2, command2+41);
    printf("command: %s\n", finalCommand2);
    system(finalCommand1);
    system(finalCommand2);
    free(privateKey);
    free(publicKey);
    free(finalCommand2);
    free(finalCommand1);
}
